import React from 'react'
import { Route, Routes } from 'react-router'
import { Home } from '../pages/public/Home'
import { Login } from '../pages/public/Login'
import { Noticias } from '../pages/public/Noticias'
import { NotFound } from '../pages/NotFound'

export const UnauthRouter = () => {
    return (
        <Routes>
            <Route path="/" element={<Home/>}>
                <Route index element={<Noticias/>}/>
                <Route path="login" element={<Login/>}/>
                <Route path="*" element={<NotFound/>}/>
            </Route>
        </Routes>
    )
}
