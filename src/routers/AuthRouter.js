import React from 'react'
import { Route, Routes } from 'react-router'
import { NotFound } from '../pages/NotFound'
import { CrearNoticias } from '../pages/private/CrearNoticias'
import { Dashboard } from '../pages/private/Dashboard'
import { Perfil } from '../pages/private/Perfil'

export const AuthRouter = () => {
    return (
        <Routes>
            <Route path="/" element={<Dashboard/>}>
                <Route index element={<CrearNoticias/>}/>
                <Route path="perfil" element={<Perfil/>} />
                <Route path="*" element={<NotFound/>}/>
            </Route>
        </Routes>
    )
}
