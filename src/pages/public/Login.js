import React, { useContext } from 'react'
import { useNavigate } from 'react-router';
import AuthContext from '../../context/AuthContext';

export const Login = () => {   
    const {handleAuth} = useContext(AuthContext);

    const navigate = useNavigate();

    const handleNoticias = ()=>{
        navigate('/');
    }

    const handleSubmit = (e)=>{
        e.preventDefault();
        console.log("login");
        handleAuth(true);
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="Usuario"/>
                <br/>
                <input type="password" placeholder="Contraseña"/>
                <br/>
                <button type="submit">Iniciar sesión</button>                
            </form>
            <br/>
            <button onClick={handleNoticias}>Ir a noticias</button>
        </div>
    )
}
