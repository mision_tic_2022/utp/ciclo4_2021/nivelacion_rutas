import React from 'react'

export const Perfil = () => {

    const handleSubmit = (e)=>{
        e.preventDefault();
    }
    
    return (
        <div>
            <h2>Perfil</h2>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="Nombre"/>
                <br/>
                <input type="text" placeholder="Apellido"/>
                <br/>
                <input type="text" placeholder="Email"/>
                <br/>
                <button>Actualizar</button>
            </form>
        </div>
    )
}
