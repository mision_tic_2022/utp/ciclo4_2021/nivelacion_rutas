import React from 'react'

export const CrearNoticias = () => {

    const handleSubmit = (e)=>{
        e.preventDefault();
    }

    return (
        <div>
            <h2>Crear Noticia</h2>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="Titulo de la noticia"/>
                <br/>
                <textarea/>
                <br/>
                <button>Crear Noticia</button>
            </form>
        </div>
    )
}
